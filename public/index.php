<?php

require_once(__DIR__.'/../app/controller/PlantController.php');

$cnt = new PlantController();
$plants = $cnt->listPlants();

?><html>
  <head>
    <title>Plants App Main page</title>
  </head>
  <body>
    <div id="wrapper">
      <h1>Plants list</h1>
      <a href="/add.php">Add plant</a>
      <ul>
      <?php foreach($plants as $p){ ?>
        <li><a href="/details.php?planta=<?=$p->getId()?>"><?=$p->getName()?></a> (<em><?=$p->getSciName()?></em>) - <?=$p->getType()?> - <a href="/update.php?planta=<?=$p->getId()?>">Update</a> - <a href="/delete.php?planta=<?=$p->getId()?>">Delete</a></li>
      <?php } ?>
      </ul>
    </div>
  </body>
</html>