<?php

require_once(__DIR__.'/../app/controller/PlantController.php');

$plid = $_GET['planta'];
$cnt = new PlantController();

$plant = $cnt->getPlant($plid);

?><html>
  <head>
    <title>Plant delete</title>
  </head>
  <body>
    <div id="wrapper">
      <h1>Delete Plant: <?=$plant->getName()?></h1>
      <h2><em><?=$plant->getSciName()?></em></h2>
      <p>Are you sure you want to delete this plant?</p>
      <form method="post" action="/forms/delete.php">
          <dt><input type="hidden" value="<?=$plant->getId()?>" name="plid"/></dt>
          <dd><input type="submit" value="Delete" name="plsub"/></dd>
        </dl>
      </form>
      <a href="/">Back home</a>
    </div>
  </body>
</html>