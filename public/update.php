<?php

require_once(__DIR__.'/../app/controller/PlantController.php');

$plid = $_GET['planta'];
$cnt = new PlantController();

$ptypes =$cnt->getPlantTypes(); 
$plant = $cnt->getPlant($plid);

?><html>
  <head>
    <title>Plants Update</title>
  </head>
  <body>
    <div id="wrapper">
      <h1>Update Plant: <?=$plant->getName()?></h1>
      <form method="post" action="/forms/update.php">
        <dl>
          <dt><label for="pname">Nom</label></dt>
          <dd><input name="pln" id="pname" type="text" value="<?=$plant->getName()?>"/></dd>
          <dt><label for="pnamec">Nom Científic</label></dt>
          <dd><input name="plnc" id="pnamec" type="text" value="<?=$plant->getSciName()?>"/></dd>
          <dt><label for="pltype">Tipus</label></dt>
          <dd>
            <select name="plt" id="pltype">
            <?php foreach($ptypes as $pt){ ?>
              <option value="<?=$pt['idtype']?>" <?php if($pt['idtype'] == $plant->getTypeId()){ echo 'selected'; }?>><?=$pt['nametype']?></option>
            <?php } ?>
            </select></dd>
          <dt><label for="ptrue">Perenne</label></dt>
          <dd>
            <input type="radio" id="ptrue" name="plp" value="true" <?php if($plant->getCad()){ echo 'checked'; } ?>>
            <label for="ptrue">Sí</label><br>
            <input type="radio" id="pfalse" name="plp" value="false" <?php if(!$plant->getCad()){ echo 'checked'; } ?>>
            <label for="pfalse">No</label><br>
          </dd>
          <dt><input type="hidden" value="<?=$plant->getId()?>" name="plid"/></dt>
          <dd><input type="submit" value="Update" name="plsub"/></dd>
        </dl>
      </form>
      <a href="/">Back home</a>
    </div>
  </body>
</html>