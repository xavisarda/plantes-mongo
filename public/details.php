<?php

require_once(__DIR__.'/../app/controller/PlantController.php');

$plid = $_GET['planta'];
$cnt = new PlantController();

$plant = $cnt->getPlant($plid);

?><html>
  <head>
    <title>Plant Information</title>
  </head>
  <body>
    <div id="wrapper">
      <h1>Plant information: <?=$plant->getName()?></h1>
      <h2><em><?=$plant->getSciName()?></em></h2>
      <hr/>
      <dl>
        <dt>Tipus</dt>
        <dd><?=$plant->getType()?></dd>
        <dt>Fulla caduca</dt>
        <dd><?php echo $plant->getCad() ? "Sí" : "No"; ?></dd>
      </dl>
      <a href="/update.php?planta=<?=$plid?>">Update plant</a>
      <a href="/">Back home</a>
    </div>
  </body>
</html>