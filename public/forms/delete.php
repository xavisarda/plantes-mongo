<?php

require_once(__DIR__.'/../../app/inc/constants.php');
require_once(__DIR__.'/../../app/controller/PlantController.php');

//RECUPEREM DADES
$plid = $_POST['plid'];

$cnt = new PlantController();
$plant = $cnt->deletePlant($plid);

?><html>
  <head>
    <title>Deleted Plant</title>
  </head>
  <body>
    <h1>Deleted Plant: <?=$plant->getName()?> (<?=$plant->getSciName()?>)</h1>
    <ul>
      <li><?=$plant->getType()?>
    </ul>
    <a href="/">Back to home</a>
  </body>
</html>