<?php

require_once(__DIR__.'/../app/controller/PlantController.php');

$cnt = new PlantController();
$ptypes = $cnt->getPlantTypes();

?><html>
  <head>
    <title>Plants App Add page</title>
  </head>
  <body>
    <div id="wrapper">
      <h1>Add Plants</h1>
      <form method="post" action="/forms/add.php">
        <dl>
            <dt><label for="pname">Nom</label></dt>
            <dd><input name="pln" id="pname" type="text"/></dd>
            <dt><label for="pnamec">Nom Científic</label></dt>
            <dd><input name="plnc" id="pnamec" type="text"/></dd>
            <dt><label for="pltype">Tipus</label></dt>
            <dd>
                <select name="plt" id="pltype">
                <?php $first = true;
                foreach($ptypes as $pt){ ?>
                  <option value="<?=$pt['idtype']?>" <?php if($first){ echo 'selected'; $first = false; }?>><?=$pt['nametype']?></option>
                <?php } ?>
                </select></dd>
            <dt><label for="ptrue">Perenne</label></dt>
            <dd>
                <input type="radio" id="ptrue" name="plp" value="true" checked>
                <label for="ptrue">Sí</label><br>
                <input type="radio" id="pfalse" name="plp" value="false">
                <label for="pfalse">No</label><br>
            </dd>
            <dt>&nbsp;</dt>
            <dd><input type="submit" value="Add" name="plsub"/></dd>
        </dl>
      </form>
    </div>
  </body>
</html>