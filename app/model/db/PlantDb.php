<?php 

require_once(__DIR__.'/../../ext/vendor/autoload.php');
require_once(__DIR__.'/../../inc/constants.php');
require_once(__DIR__.'/../Plant.php');
require_once(__DIR__.'/../../inc/ConnectionManager.php');

class PlantDb{

  private $_conn;

  /**
   * return Plant Array
   */
  public function listPlants(){
    $cm = ConnectionManager::getConnectionManager();
    //$collection = $this->openConnection();
    $collection = $cm->connectPlants();
    $ps = $collection->find();

    $plants = array();
    foreach($ps as $p){
      array_push($plants, new Plant($p['name'], $p['sci_name'], 
              $p['type']['planttype'],$p['type']['idplanttype'],
              $p['cad'],$p['_id']->__toString()));
    }
    return $plants;
  }

  /**
   * idtype and nametype are keys
   * 
   * return mixed array
   */
  public function listPlantTypes(){
    $cm = ConnectionManager::getConnectionManager();
    //$collection = (new MongoDB\Client)->plantes->planttype;
    $collection = $cm->connectPlantType();
    $pts = $collection->find();

    $planttypes = array();
    foreach($pts as $pt){
      array_push($planttypes, ["idtype" => $pt['_id']->__toString(), 
          "nametype" => $pt['plant_type']]);
    }
    return $planttypes;
  }

  /**
   * 
   */
  public function getPlantType($id){
    $cm = ConnectionManager::getConnectionManager();
    //$collection = (new MongoDB\Client)->plantes->planttype;
    $collection = $cm->connectPlantType();
    $pt = $collection->findOne(['_id' => new MongoDB\BSON\ObjectId($id)]);
    return ["idtype" => $pt['_id']->__toString(),
        "nametype" => $pt['plant_type']];
  }

  /**
   * return Plant
   */
  public function getPlant($id){
    $cm = ConnectionManager::getConnectionManager();
    //$collection = $this->openConnection();
    $collection = $cm->connectPlants();
    $p = $collection->findOne(['_id' => new MongoDB\BSON\ObjectId($id)]);

    return new Plant($p['name'], $p['sci_name'], 
        $p['type']['planttype'],$p['type']['idplanttype'],
        $p['cad'],$p['_id']->__toString());

  }

  /**
   * return Plant
   */
  public function addPlant($n, $nc, $t, $p){
    $cm = ConnectionManager::getConnectionManager();
    //$collection = $this->openConnection();
    $collection = $cm->connectPlants();
    $type = $this->getPlantType($t);
      
    $planta = new Plant($n, $nc, $type['nametype'], $t, $p);
    $insertP = $collection->insertOne($planta->toArray());

    return $this->getPlant($insertP->getInsertedId());
  }

  /**
   * return Plant
   */
  public function updatePlant($n, $nc, $t, $p, $id){
    $type = $this->getPlantType($t);
    $plant = new Plant($n, $nc, $type['nametype'], $t, $p);
    $updatestmt = [ '$set' => $plant->toArray() ];
    $updatefilter =  ['_id' => new MongoDB\BSON\ObjectId($id)];
    $cm = ConnectionManager::getConnectionManager();
    //$collection = $this->openConnection();
    $collection = $cm->connectPlants();

    $updatep = $collection->updateOne($updatefilter, $updatestmt);
    return $this->getPlant($id);
  }

  /**
   * return Plant
   */
  public function removePlant($id){
    $cm = ConnectionManager::getConnectionManager();
    //$collection = $this->openConnection();
    $collection = $cm->connectPlants();
    $plant = $this->getPlant($id);

    $removep = $collection->deleteOne(['_id' => new MongoDB\BSON\ObjectId($id)]);
    return $plant;
  }

  /**
   * @deprecated
   */
  private function openConnection(){
      return (new MongoDB\Client)->plantes->planta;
  }
    
}
