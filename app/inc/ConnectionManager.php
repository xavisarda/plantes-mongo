<?php

require_once(__DIR__.'/../ext/vendor/autoload.php');


/**
 * Singleton implementation class
 * 
 */
class ConnectionManager{

  private static $ins;

  private function __construct(){ }

  public static function getConnectionManager(){
    if(self::$ins == null){
      self::$ins = new ConnectionManager();
    }
    return self::$ins;
  }

  public function connectPlants(){
    return (new MongoDB\Client)->plantes->planta;
  }

  public function connectPlantType(){
    return (new MongoDB\Client)->plantes->planttype;
  }

}
